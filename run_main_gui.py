from gui_layouts import main_gui_code
from PySide.QtCore import *
from PySide.QtGui import *
import sys


class MainGUI(QDialog, main_gui_code.Ui_Dialog):

    """ This set will contain all items for the QListWidget.
        It's purpose is to make managing list items easier.
        Any time you add/remove from this, you must call
            self.update_lists() to update the screen
    """
    list_products_items = set()

    def __init__(self, parent=None):
        # Any additions you wish to add to the init, do it after self.setupUi(self)
        super(MainGUI, self).__init__(parent)
        self.setupUi(self)

        self.list_products.setContextMenuPolicy(Qt.CustomContextMenu)
        self.list_products.connect(self.list_products,
                                   SIGNAL("customContextMenuRequested(QPoint)"),
                                   self.list_products_right_click_menu)

        # When the QListWidget list_products is double clicked on, connect it to the
        #   self.product_list_double_clicked_remove method
        self.list_products.itemDoubleClicked.connect(self.product_list_double_clicked_remove)

        # Here I am pre-adding items to the QListWidget
        self.list_products_items.add('Added from __init__')

        # Since I have modified the list_products_items set, I need to update the QListWidget on the GUI
        self.update_lists()

        """
        Something you may notice is that the "Added via Designer" list item disappeared.

        This occurred because in this example we're basing the contents of the QListWidget on
            our own self.list_products_items set(). I would not recommend adding the items to
            QListWidget's using the designer, add them here in this code.

        """

    @Slot()
    def on_btn_add_product_clicked(self):

        # Obtain the text that the user put into the QLineEdit
        text_entered = self.txt_new_product.text()

        # Do not add empty/blank text entered.
        if not text_entered:
            return

        # Check to see if the text entered already exists in our items list
        # If so, pop-up a message saying so
        # However, because set() will only contain distinct values, there really is no
        #   need to stop this methods execution, just continue processing.
        if text_entered in self.list_products_items:
            self.simple_popup('{} already exists in the list'.format(text_entered), 'Already Exists')

        # Add the text to the set for the QListWidget
        self.list_products_items.add(text_entered)

        # Update the QListWidget
        self.update_lists()

        # Clear the text field of its contents
        self.txt_new_product.setText('')

    def product_list_double_clicked_remove(self):
        """ Double clicking on a QListWidget item will trigger this method.

        In this example we will remove the item from the list
        """

        # Loop over each item selected. You can within QtDesigner set a list to MultiSelect mode.
        for selected_item in self.list_products.selectedItems():

            # Remove the item from the list_products_items set()
            self.list_products_items.remove(selected_item.text())

        # Update the QListWidget
        self.update_lists()

    def update_lists(self):
        """ Update the QListWidget on the dialog screen"""

        # Clear anything from the current QListWidget
        self.list_products.clear()

        # Want an alphabetically sorted list in the QListWidget?
        sorted_product_items = sorted(self.list_products_items)

        # Add each item to the QListWidget itself
        for item in sorted_product_items:
            self.list_products.addItem(item)

    def list_products_right_click_menu(self, position):
        """ Sets the right click menu items for the list_products table

        :param position:
        :return:
        """
        self.list_menu = QMenu()
        menu_item = self.list_menu.addAction("Remove Product")

        # Connect this menu item to the product removal method.
        self.connect(menu_item, SIGNAL("triggered()"), self.product_list_double_clicked_remove)

        # We need to also assign the menus location on the screen, this calculates that.
        # If you use this method for another list box, do not forget to update the self.list_products.mapToGlobal.
        parent_position = self.list_products.mapToGlobal(QPoint(0, 0))
        self.list_menu.move(parent_position + position)
        self.list_menu.show()

    # Using @staticmethod because this pop-up will never call a self. method.
    @staticmethod
    def simple_popup(message, title='Alert', yesno=None):
        """ Causes a simple QMessageBox to appear alerting the user to something

        :param message: string containing your message
        :param title: string containing popup title
        :param yesno: boolean whether or not you want Yes/No buttons plus a return value
        :return: If yesno is True, returns True/False depending on users selection, otherwise void
        """
        msgBox = QMessageBox()
        msgBox.setWindowTitle(title)
        msgBox.setText(message)
        msgBox.setStandardButtons(QMessageBox.Ok)
        msgBox.setDefaultButton(QMessageBox.Ok)

        # By default we will only show an OK button.
        # If the yesno variable is True, change that to Yes and No
        #   buttons PLUS return the value the user selected as boolean.
        if yesno:
            msgBox.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
            msgBox.setDefaultButton(QMessageBox.No)

            return msgBox.exec_() == QMessageBox.Yes

        msgBox.exec_()

def main():
    app = QApplication(sys.argv)
    form = MainGUI()
    form.show()
    app.exec_()

if __name__ == '__main__':
    main()
