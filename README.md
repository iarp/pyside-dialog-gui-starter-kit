# PySide GUI using Dialog UI #

This is a starter kit for getting a GUI up and running quickly.

It is based on Dialog, NOT on Main Window. If you want a Main Window it will require a few tweaks, namely to the class MainGUI() extension.

## Running ##

All you need to do is:

```
#!python
python run_main_gui.py

```
## Requires ##

* PySide

Windows users may want to just go for the pre-compiled binaries rather than fight with Windows and compiling it yourself. You can find unoffical binaries on this page http://www.lfd.uci.edu/~gohlke/pythonlibs/#pyside

## Includes ##

* QLineEdit
* QPushButton
* QListWidget
* On button clicked
* Double clicking a QListWidget item. In this example we remove an item from the QListWidget.
* Simple popup using QMessageBox with OK or Yes/No buttons