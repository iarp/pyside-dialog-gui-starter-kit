# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main_gui.ui'
#
# Created: Tue Jun  2 10:26:36 2015
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(283, 323)
        self.txt_new_product = QtGui.QLineEdit(Dialog)
        self.txt_new_product.setGeometry(QtCore.QRect(10, 10, 181, 20))
        self.txt_new_product.setInputMask("")
        self.txt_new_product.setObjectName("txt_new_product")
        self.btn_add_product = QtGui.QPushButton(Dialog)
        self.btn_add_product.setGeometry(QtCore.QRect(200, 10, 71, 23))
        self.btn_add_product.setObjectName("btn_add_product")
        self.list_products = QtGui.QListWidget(Dialog)
        self.list_products.setGeometry(QtCore.QRect(10, 40, 261, 271))
        self.list_products.setObjectName("list_products")
        QtGui.QListWidgetItem(self.list_products)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Sample GUI", None, QtGui.QApplication.UnicodeUTF8))
        self.txt_new_product.setPlaceholderText(QtGui.QApplication.translate("Dialog", "Add New Product", None, QtGui.QApplication.UnicodeUTF8))
        self.btn_add_product.setText(QtGui.QApplication.translate("Dialog", "Add Product", None, QtGui.QApplication.UnicodeUTF8))
        __sortingEnabled = self.list_products.isSortingEnabled()
        self.list_products.setSortingEnabled(False)
        self.list_products.item(0).setText(QtGui.QApplication.translate("Dialog", "Added via Designer", None, QtGui.QApplication.UnicodeUTF8))
        self.list_products.setSortingEnabled(__sortingEnabled)

